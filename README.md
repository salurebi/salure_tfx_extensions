# Salure TFX Extensions

Repository: https://bitbucket.org/salurebi/salure_tfx_extensions/src/master/

Docker Image over at hub.docker.com: salure/kubeflow_tfx_extensions


## Goal
Two goals:

- To add functionality as required by internal demand of Salure.
- To provide helper functions and new components for TFX

